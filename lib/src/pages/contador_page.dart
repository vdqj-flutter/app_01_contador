import 'package:flutter/material.dart';

class ContadorPage extends StatefulWidget {
  const ContadorPage({super.key});

  @override
  createState() {
    return _ContadorPageState();
  }
}

class _ContadorPageState extends State<ContadorPage> {
  final _estiloTexto = const TextStyle(fontSize: 25);
  int _conteo = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('StateFull'),
        centerTitle: true,
        elevation: 1.4,
      ),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text('Hola Mundo', style: _estiloTexto),
          Text('$_conteo', style: _estiloTexto),
        ],
      )),
      floatingActionButton: _crearBotones(),
    );
  }

  Widget _crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        const SizedBox(
          width: 30,
        ),
        FloatingActionButton(
            onPressed: _reiniciar, child: const Icon(Icons.exposure_zero)),
        const Expanded(child: SizedBox()),
        FloatingActionButton(
            onPressed: _decrementar, child: const Icon(Icons.remove)),
        const SizedBox(
          width: 5,
        ),
        FloatingActionButton(
          onPressed: _incrementar,
          tooltip: 'Increment',
          child: const Icon(Icons.add),
        )
      ],
    );
  }

  void _incrementar() {
    setState(() {
      _conteo++;
    });
  }

  void _decrementar() {
    setState(() => _conteo--);
  }

  void _reiniciar() {
    setState(() => _conteo = 0);
  }
}
