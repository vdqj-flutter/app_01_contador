import 'package:app_01_contador/src/pages/contador_page.dart';
import 'package:flutter/material.dart';

// MY SCRIPTS
// import 'package:app_01_contador/src/pages/home_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // Sobre escribir el metodo build
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false, // quitar baner de debug
      //home: HomePage(),
      home: ContadorPage(),
    );
  }
}
